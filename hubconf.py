dependencies = ['torch']
from xtransformer import xtransformer as _xtransformer

def xtransformer(pretrained=True, **kwargs):
    """ # xtransformer model for abkhazian to russian and back
    pretrained (bool): kwargs, load pretrained weights into the model
    """
    dirname = os.path.dirname(__file__)
    checkpoint = os.path.join(dirname, "processed/checkpoint_best.pt")
    state_dict = torch.load(checkpoint)

    # Call the model, load pretrained weights
    model = _xtransformer(pretrained=pretrained, **kwargs)
    model.load_state_dict(state_dict)
    return model
