# Abkazian Language Processing

This work is made possible by the [Abkhaz monolingual corpus](http://clarino.uib.no/abnc/page), the [Abkhazian Language Committee](http://apsua.tk/), the [multilingual parallel corpus](https://github.com/danielinux7/Multilingual-Parallel-Corpus) and is proudly supported by [multisource](https://bachstelze.gitlab.io/multisource/) in commemoration of my godmother Dr. Esther König.


Tools and models to work with the abkhazian language:
- [Abkhazian-Russian translation model](https://colab.research.google.com/drive/1tE2byVTR-sSevlSM_t1h-xFuK2IKZBwN?usp=sharing) based on the current training weights in the processed folder
- `abkhaz_tokenizer.pickle` as abkhazian sentence tokenizer
- `abkhaz.vec` 	and `abkhaz.bin` as abkhazian [fasttext](https://fasttext.cc/docs/en/crawl-vectors.html) embedding vectors and binaries
- `abkhaz.model` and `abkhaz.vec` as abkhazian bpe model and vocaby for sentencepiece

## Project plan:
- [x] Digitalize a Russian-Abkhazian dictionary
- [x] Gather a monolingual and bilingual corpus
- [x] Build a sentence tokenizer
- [x] Paraphrase generation
- [x] Pretrain a bilingual translation model

- [ ] Align further [translation data](https://github.com/danielinux7/Multilingual-Parallel-Corpus)
- [ ] Train and fine-tune a bilingual translation model
- [ ] Train a languagel model
- [ ] Generate a filterd back-translation corpus
- [ ] Collect a multilingual corpus
- [ ] Train a multilingual translation model
- [ ] Increase the [common voice](https://voice.mozilla.org/ab) database


Feel free to contribute and make a language request for the multilingual translation model.